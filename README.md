# Advent of Code 2021

All my solutions for this year's [advent of code](https://adventofcode.com/2021) tasks. Written in Java.

## Problems:

- [Day 01](./src/Day01.java) Count increments in a list.
- [Day 02](./src/Day02.java) Calculate depth to the seafloor.
- [Day 03](./src/Day03.java) Count occurrences in binary numbers.
- [Day 04](./src/Day04.java) Play Bingo.
- [Day 05](./src/Day05.java) Find intersecting lines.
- [Day 06](./src/Day06.java) Simulate a fish population.
- [Day 07](./src/Day07.java) Minimal diff for a list of numbers.
- [Day 08](./src/Day08.java) Parse corrupt seven-segment displays.
- [Day 09](./src/Day09.java) Find low points/basins in a matrix.
- [Day 10](./src/Day10.java) Parse different opening and closing brackets.
- [Day 11](./src/Day11.java) Simulate flashes in a matrix.
- [Day 12](./src/Day12.java) Find number of paths in a graph.
- [Day 13](./src/Day13.java) Fold a transparent paper multiple times.
- [Day 14](./src/Day14.java) Insert characters in a string.
- [Day 15](./src/Day15.java) Find the shortest path in a matrix.
- [Day 16](./src/Day16.java) Decode some funny code.
- [Day 17](./src/Day17.java) Simulate a shot into a target area.
- [Day 18](./src/Day18.java) Build, merge and modify binary trees.
- [Day 19](./src/Day19.java) Find rotation and translation between multiple sets of points.
- [Day 20](./src/Day20.java) Run an image enhancement algorithm multiple times.
- [Day 21](./src/Day21.java) Play a game with a die.
- [Day 22](./src/Day22.java) Track active regions of a grid by cutting cubes.
- [Day 23](./src/Day23.java) Simulate a small game sorting 4 stacks move-by-move.
- [Day 24](./src/Day24.java) Run an ALU to calculate the smallest or largest number.
- [Day 25](./src/Day25.java) Simulate some sea cucumbers.
