import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day17 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-17.txt"));
        Pattern pattern = Pattern.compile("x=(-?\\d+)..(-?\\d+), y=(-?\\d+)..(-?\\d+)");
        String line = scanner.nextLine();
        Matcher matcher = pattern.matcher(line);
        matcher.find();

        int x0 = Integer.parseInt(matcher.group(1));
        int x1 = Integer.parseInt(matcher.group(2));
        int y0 = Integer.parseInt(matcher.group(3));
        int y1 = Integer.parseInt(matcher.group(4));

        System.out.println("Part 1: " + partOne(x0, x1, y0, y1));
        System.out.println("Part 2: " + partTwo(x0, x1, y0, y1));
    }

    private static int partOne(int x0, int x1, int y0, int y1) {
        return (Math.abs(y0) * (Math.abs(y0) - 1)) / 2;
    }

    private static int partTwo(int x0, int x1, int y0, int y1) {
        int counter = 0;
        // for x in Math.sqrt(x0 * 2)..x1
        for (int x = (int) Math.floor(Math.sqrt(x0 * 2)); x <= x1; x++) {
            // for y in -y0..(y0 - 1)
            for (int y = y0; y < Math.abs(y0); y++) {
                // if it reaches target area -> counter++
                int i = 0;
                int veloX = x;
                int j = 0;
                int veloY = y;
                while (i <= x1 && j >= y0) {
                    i += veloX;
                    j += veloY;

                    if (veloX > 0) {
                        veloX -= 1;
                    }
                    veloY -= 1;

                    // if (i,j) is in target area
                    if (i >= x0 && i <= x1 && j >= y0 && j <= y1) {
                        counter++;
                        System.out.printf("%d,%d%n", x, y);
                        break;
                    }
                }
            }
        }

        return counter;
    }
}
