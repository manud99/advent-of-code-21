import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Day04 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-04.txt"));

//        int result = partOne(scanner);
        int result = partTwo(scanner);

        System.out.println("result = " + result);
    }

    private static int partOne(Scanner scanner) {
        String[] numbers = scanner.nextLine().split(",");
        ArrayList<Board> boards = parseBoards(scanner);

        for (String number : numbers) {
            int n = Integer.parseInt(number);
            for (Board board : boards) {
                board.mark(n);

                if (board.isFinished()) {
                    return board.getScore() * n;
                }
            }
        }

        return -1;
    }

    private static int partTwo(Scanner scanner) {
        String[] numbers = scanner.nextLine().split(",");
        ArrayList<Board> boards = parseBoards(scanner);
        int lastScore = -1;

        for (String number : numbers) {
            int n = Integer.parseInt(number);
            for (Board board : boards) {
                boolean wasFinished = board.isFinished();

                board.mark(n);

                if (!wasFinished && board.isFinished()) {
                    lastScore = board.getScore() * n;
                }
            }
        }

        return lastScore;
    }

    private static ArrayList<Board> parseBoards(Scanner scanner) {
        ArrayList<Board> boards = new ArrayList<>();

        while (scanner.hasNextLine()) {
            int[] board = new int[25];
            int j = 0;

            scanner.nextLine();
            for (int i = 0; i < 5; i++) {
                String line = scanner.nextLine();
                Scanner lineScanner = new Scanner(line);
                while (lineScanner.hasNextInt()) {
                    board[j++] = lineScanner.nextInt();
                }
                lineScanner.close();
            }

            boards.add(new Board(board));
        }
        return boards;
    }
}

class Board {
    int[] numbers;
    boolean[] marked;

    Board(int[] numbers) {
        this.numbers = numbers;
        this.marked = new boolean[numbers.length];
    }

    public void mark(int x) {
        int i = find(x);
        if (i == -1) {
            return;
        }

        this.marked[i] = true;
    }

    public int find(int x) {
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] == x) {
                return i;
            }
        }

        return -1;
    }

    public boolean isFinished() {
        for (int i = 0; i < 5; i++) {
            boolean hasFullColumn = true;
            boolean hasFullRow = true;

            for (int j = 0; j < 5; j++) {
                if (!marked[i * 5 + j]) {
                    hasFullRow = false;
                }
                if (!marked[j * 5 + i]) {
                    hasFullColumn = false;
                }
            }

            if (hasFullColumn || hasFullRow) return true;
        }

        return false;
    }

    public int getScore() {
        int score = 0;

        for (int i = 0; i < this.numbers.length; i++) {
            if (!this.marked[i]) {
                score += this.numbers[i];
            }
        }

        return score;
    }
}
