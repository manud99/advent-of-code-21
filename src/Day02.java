import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day02 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-02.txt"));
//        int result = partOne(scanner);
        int result = partTwo(scanner);

        System.out.println("result = " + result);
    }

    /**
     * down X increases your aim by X units.
     * up X decreases your aim by X units.
     * forward X does two things:
     *   - It increases your horizontal position by X units.
     *   - It increases your depth by your aim multiplied by X.
     */
    private static int partTwo(Scanner scanner) {
        int position = 0;
        int depth = 0;
        int aim = 0;

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            Scanner lineScanner = new Scanner(line);

            String token = lineScanner.next();
            int value = lineScanner.nextInt();

            if (token.equals("forward")) {
                position += value;
                depth += aim * value;
            } else if (token.equals("up")) {
                aim -= value;
            } else {
                aim += value;
            }
        }

        return position * depth;
    }

    private static int partOne(Scanner scanner) {
        int position = 0;
        int depth = 0;

        while (scanner.hasNextLine()) {
            // forward 8
            String line = scanner.nextLine();
            Scanner lineScanner = new Scanner(line);

            String token = lineScanner.next();
            int value = lineScanner.nextInt();

            if (token.equals("forward")) {
                position += value;
            } else if (token.equals("up")) {
                depth -= value;
            } else {
                depth += value;
            }
        }
        return position * depth;
    }
}
