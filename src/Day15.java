import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Scanner;

public class Day15 {
    private final static int n = 100;

    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-15.txt"));

        Node[][] nodes = new Node[n][n];
        for (int i = 0; i < n; i++) {
            char[] chars = scanner.nextLine().toCharArray();
            for (int j = 0; j < chars.length; j++) {
                char c = chars[j];
                nodes[i][j] = new Node(i, j, c - 48);
            }
        }

        System.out.println("Part 1: " + partOne(nodes));
        System.out.println("Part 2: " + partTwo(nodes));
    }

    private static int partOne(Node[][] nodes) {
        connectNodes(nodes, n);
        return dijkstra(nodes, n);
    }

    private static int partTwo(Node[][] map) {
        final int largeN = 5 * n;
        Node[][] largeMap = new Node[largeN][largeN];
        for (int i = 0; i < n; i++) {
            largeMap[i] = Arrays.copyOf(map[i], largeN);
        }

        for (int x = 0; x < 5; x++) {
            for (int y = 0; y < 5; y++) {
                if (x == 0 && y == 0) continue;

                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < n; j++) {
                        int k = n * x + i;
                        int l = n * y + j;

                        int weight = map[i][j].weight + x + y;
                        if (weight > 9) weight %= 9;
                        if (weight == 0) weight = 9;

                        largeMap[k][l] = new Node(k, l, weight);
                    }
                }
            }
        }

        connectNodes(largeMap, largeN);

        return dijkstra(largeMap, largeN);
    }

    private static void connectNodes(Node[][] map, int n) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i < n - 1) {
                    map[i][j].children[0] = map[i + 1][j];
                }
                if (i > 0) {
                    map[i][j].children[1] = map[i - 1][j];
                }
                if (j < n - 1) {
                    map[i][j].children[2] = map[i][j + 1];
                }
                if (j > 0) {
                    map[i][j].children[3] = map[i][j - 1];
                }
            }
        }
    }

    private static int dijkstra(Node[][] nodes, int n) {
        PriorityQueue<Pair> pq = new PriorityQueue<>();
        boolean[][] inS = new boolean[n][n];
        int[][] d = new int[n][n];
        int sizeOfS = 0;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0 && j == 0) {
                    pq.add(new Pair(nodes[0][0], 0));
                    d[0][0] = 0;
                    continue;
                }

                pq.add(new Pair(nodes[i][j], Integer.MAX_VALUE));
                d[i][j] = Integer.MAX_VALUE;
            }
        }

        while (sizeOfS < n * n && !pq.isEmpty()) {
            Pair pair = pq.poll();

            inS[pair.index.i][pair.index.j] = true;
            sizeOfS++;

            for (Node child : nodes[pair.index.i][pair.index.j].children) {
                if (child == null || inS[child.i][child.j]) continue;

                if (d[child.i][child.j] > d[pair.index.i][pair.index.j] + child.weight) {
                    d[child.i][child.j] = d[pair.index.i][pair.index.j] + child.weight;
                    pq.add(new Pair(child, d[child.i][child.j]));
                }
            }
        }

        return d[n - 1][n - 1];
    }

    private static void printMap(Node[][] map) {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                System.out.print(map[i][j].weight);
            }
            System.out.println();
        }
    }
}

class Node {
    final int i, j;
    final int weight;
    final Node[] children = new Node[4];

    Node(int i, int j, int weight) {
        this.i = i;
        this.j = j;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return String.format("%d/%d/%d", i, j, weight);
    }
}

class Pair implements Comparable<Pair> {
    Node index;
    int value;

    public Pair(Node index, int value) {
        this.index = index;
        this.value = value;
    }

    @Override
    public String toString() {
        return String.format("%d/%d/%d", index.i, index.j, value);
    }

    @Override
    public int compareTo(Pair o) {
        return Integer.compare(this.value, o.value);
    }
}
