import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day23 {
    private static final int[] queueToHallway = {2, 4, 6, 8};

    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-23.txt"));
        scanner.nextLine();

        String hallwayLine = scanner.nextLine();
        int hallwayLength = hallwayLine.substring(1, hallwayLine.length() - 1).length();
        int[] hallway = new int[hallwayLength];
        Arrays.fill(hallway, -1);

        int[][] queues = new int[4][2];
        for (int i = 0; i < 2; i++) {
            Pattern pattern = Pattern.compile("[# ]+([ABCD])#([ABCD])#([ABCD])#([ABCD])#+");
            Matcher matcher = pattern.matcher(scanner.nextLine());

            if (matcher.matches()) {
                queues[0][i] = matcher.group(1).charAt(0) - 'A';
                queues[1][i] = matcher.group(2).charAt(0) - 'A';
                queues[2][i] = matcher.group(3).charAt(0) - 'A';
                queues[3][i] = matcher.group(4).charAt(0) - 'A';
            }
        }

        System.out.println("Part 1: " + partOne(hallway, queues));
        System.out.println("Part 2: " + partTwo(hallway, queues));
    }

    private static int partOne(int[] hallway, int[][] queues) {
        return dijkstra(hallway, queues);
    }

    private static int partTwo(int[] hallway, int[][] queues) {
        int[][] extendedQueues = new int[4][4];
        int[] firstRow = {3, 2, 1, 0};
        int[] secondRow = {3, 1, 0, 2};

        for (int i = 0; i < extendedQueues.length; i++) {
            for (int j = 0; j < extendedQueues[i].length; j++) {
                switch (j) {
                    case 0:
                        extendedQueues[i][j] = queues[i][0];
                        break;
                    case 1:
                        extendedQueues[i][j] = firstRow[i];
                        break;
                    case 2:
                        extendedQueues[i][j] = secondRow[i];
                        break;
                    case 3:
                        extendedQueues[i][j] = queues[i][1];
                        break;
                }
            }
        }

        return dijkstra(hallway, extendedQueues);
    }

    private static int dijkstra(int[] hallway, int[][] queues) {
        PriorityQueue<Pair> pq = new PriorityQueue<>();
        Map<State, Integer> scores = new HashMap<>();
        Set<State> visited = new HashSet<>();
        State startStep = new State(hallway, queues);

        pq.add(new Pair(startStep, 0));
        scores.put(startStep, 0);
        visited.add(startStep);

        while (!pq.isEmpty()) {
            Pair current = pq.poll();
            visited.add(current.state);

            if (current.state.isFinished()) {
//                State state = current.state;
//                while (state != null) {
//                    state.print();
//                    state = state.prev;
//                }

                return current.score;
            }

            for (Pair next : current.state.getNextPossibleStates()) {
                if (visited.contains(next.state)) continue;

                int newScore = current.score + next.score;
                if (newScore < scores.getOrDefault(next.state, Integer.MAX_VALUE)) {
                    next.score = newScore;
                    next.state.prev = current.state;

                    pq.add(next);
                    scores.put(next.state, newScore);
                }
            }
        }

        return -1;
    }

    private static class State {
        int[] hallway;
        int[][] queues;
        State prev;

        public State(int[] hallway, int[][] queues) {
            this.hallway = hallway;
            this.queues = queues;
        }

        public List<Pair> getNextPossibleStates() {
            List<Pair> list = new ArrayList<>();

            // for each block in the hallway
            for (int i = 0; i < hallway.length; i++) {
                int block = hallway[i];
                // if queue allows it and if hallway is not blocked in between
                if (block == -1
                        || isQueueFinished(block) || !isQueuePartiallyComplete(block)
                        || hallwayIsBlocked(i, queueToHallway[block]))
                    continue;

                int dest = firstOpenPositionInQueue(block);

                int[] h = Arrays.copyOf(hallway, hallway.length);
                h[i] = -1;
                int[][] q = deepCopy(queues);
                q[block][dest] = block;

                // calculate score
                int s = ((Math.abs(queueToHallway[block] - i) + (1 + dest)) * (int) Math.pow(10, block));

                list.add(new Pair(new State(h, q), s));
            }

            // for each top block in each queue
            for (int i = 0; i < queues.length; i++) {
                // if queue is not empty or (partially) complete
                if (isQueuePartiallyComplete(i)) continue;

                int origin = firstFilledPositionInQueue(i);
                int block = queues[i][origin];

                for (int j = 0; j < hallway.length; j++) {
                    if (hallway[j] != -1
                            || j == 2 || j == 4 || j == 6 || j == 8
                            || hallwayIsBlocked(queueToHallway[i], j))
                        continue;

                    int[] h = Arrays.copyOf(hallway, hallway.length);
                    h[j] = block;
                    int[][] q = deepCopy(queues);
                    q[i][origin] = -1;

                    int s = ((Math.abs(queueToHallway[i] - j) + (1 + origin)) * (int) Math.pow(10, block));

                    list.add(new Pair(new State(h, q), s));
                }
            }

            return list;
        }

        private int firstOpenPositionInQueue(int queueNo) {
            for (int i = queues[queueNo].length - 1; i > 0; i--) {
                if (queues[queueNo][i] == -1) return i;
            }
            return 0;
        }

        private int firstFilledPositionInQueue(int queueNo) {
            for (int i = 0; i < queues[queueNo].length; i++) {
                if (queues[queueNo][i] != -1) return i;
            }
            return -1;
        }

        public boolean isFinished() {
            for (int i = 0; i < queues.length; i++) {
                if (!isQueueFinished(i)) return false;
            }
            return true;
        }

        private boolean isQueueFinished(int queueNo) {
            for (int q : queues[queueNo]) {
                if (q != queueNo) return false;
            }
            return true;
        }

        private boolean isQueuePartiallyComplete(int queueNo) {
            for (int q : queues[queueNo]) {
                if (q != queueNo && q != -1) return false;
            }
            return true;
        }

        private boolean isQueueEmpty(int queueNo) {
            for (int q : queues[queueNo]) {
                if (q != -1) return false;
            }
            return true;
        }

        private boolean hallwayIsBlocked(int from, int to) {
            for (int i = Math.min(from, to) + 1; i < Math.max(from, to); i++) {
                if (hallway[i] != -1) return true;
            }
            return false;
        }

        private static int[][] deepCopy(int[][] original) {
            int[][] copy = new int[original.length][];
            for (int i = 0; i < original.length; i++) {
                copy[i] = Arrays.copyOf(original[i], original[i].length);
            }
            return copy;
        }

        public void print() {
            System.out.println("#".repeat(hallway.length + 2));
            System.out.print("#");
            for (int i : hallway) {
                if (i == -1) System.out.print(".");
                else System.out.print((char) (i + 'A'));
            }
            System.out.println("#");

            for (int i = 0; i < queues[0].length; i++) {
                if (i == 0) System.out.print("###");
                else System.out.print("  #");

                for (int[] queue : queues) {
                    if (queue[i] == -1) System.out.print(".");
                    else System.out.print((char) (queue[i] + 'A'));
                    System.out.print("#");
                }

                if (i == 0) System.out.print("##");
                System.out.println();
            }

            System.out.println("  " + "#".repeat(9));
            System.out.println();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            State state = (State) o;
            return Arrays.equals(hallway, state.hallway) && Arrays.deepEquals(queues, state.queues);
        }

        @Override
        public int hashCode() {
            int result = Arrays.hashCode(hallway);
            result = 31 * result + Arrays.deepHashCode(queues);
            return result;
        }
    }

    private static class Pair implements Comparable<Pair> {
        State state;
        int score;

        public Pair(State state, int score) {
            this.state = state;
            this.score = score;
        }

        @Override
        public int compareTo(Pair o) {
            return Integer.compare(score, o.score);
        }
    }
}
