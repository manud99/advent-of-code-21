import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class Day09 {

    private static final int n = 100;

    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-09.txt"));
        int[][] matrix = new int[n][n];

        int i = 0;
        while (scanner.hasNextLine()) {
            int j = 0;
            for (char c : scanner.nextLine().toCharArray()) {
                matrix[i][j++] = c - 48;
            }
            i++;
        }

        System.out.println("Part 1: " + partOne(matrix));
        System.out.println("Part 2: " + partTwo(matrix));
    }

    private static int partOne(int[][] matrix) {
        boolean[][] lowPoints = new boolean[n][n];

        // check all rows
        for (int i1 = 0; i1 < n; i1++) {
            for (int j1 = 0; j1 < n; j1++) {
                lowPoints[i1][j1] = (i1 <= 0 || matrix[i1 - 1][j1] > matrix[i1][j1]) && (i1 >= n - 1 || matrix[i1 + 1][j1] > matrix[i1][j1]);
            }
        }

        // check all columns if lowPoint was true
        for (int i1 = 0; i1 < n; i1++) {
            for (int j1 = 0; j1 < n; j1++) {
                if (lowPoints[i1][j1]) {
                    lowPoints[i1][j1] = (j1 <= 0 || matrix[i1][j1 - 1] > matrix[i1][j1]) && (j1 >= n - 1 || matrix[i1][j1 + 1] > matrix[i1][j1]);
                }
            }
        }

        // find sum
        int sum = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (lowPoints[i][j]) {
                    sum += matrix[i][j] + 1;
                }
            }
        }

        return sum;
    }

    private static int partTwo(int[][] matrix) {
        ArrayList<Integer> sizes = new ArrayList<>();
        boolean[][] checked = new boolean[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (!checked[i][j] && matrix[i][j] != 9) {
                    sizes.add(findBasinSize(matrix, checked, i, j));
                }
            }
        }

        return sizes.stream().sorted(Comparator.reverseOrder()).limit(3).reduce((a, b) -> a * b).orElse(0);
    }

    private static int findBasinSize(int[][] matrix, boolean[][] checked, int i, int j) {
        if (i < 0 || j < 0 || i >= n || j >= n || checked[i][j] || matrix[i][j] == 9) {
            return 0;
        }

        checked[i][j] = true;

        int size = 1;
        size += findBasinSize(matrix, checked, i + 1, j);
        size += findBasinSize(matrix, checked, i - 1, j);
        size += findBasinSize(matrix, checked, i, j + 1);
        size += findBasinSize(matrix, checked, i, j - 1);

        return size;
    }
}
