import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Day13 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-13.txt"));
        boolean[][] matrix = new boolean[1350][1350];
        String[] folds = new String[12];

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();

            if (line.isEmpty()) break;

            String[] parts = line.split(",");
            int x = Integer.parseInt(parts[0]);
            int y = Integer.parseInt(parts[1]);

            matrix[x][y] = true;
        }

        for (int i = 0; scanner.hasNextLine(); i++) {
            folds[i] = scanner.nextLine();
        }

        System.out.println("Part 1: " + partOne(Arrays.copyOf(matrix, matrix.length)));
        System.out.println("Part 2: " + partTwo(Arrays.copyOf(matrix, matrix.length), folds));
    }

    private static int partOne(boolean[][] matrix) {
//        foldY(matrix, 7);
        foldX(matrix, 655);

        return countTrueValues(matrix);
    }

    private static int partTwo(boolean[][] matrix, String[] folds) {
        for (String fold : folds) {
            String[] parts = fold.split("=");
            int position = Integer.parseInt(parts[1]);

            if (fold.contains("x")) {
                foldX(matrix, position);
            } else {
                foldY(matrix, position);
            }
        }

        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 40; j++) {
                System.out.print(matrix[j][i] ? "#" : ".");
            }
            System.out.println();
        }

        return -1;
    }

    private static void foldX(boolean[][] matrix, int position) {

        for (int i = 0; i < position; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (2 * position < i) {
                    continue;
                }

                matrix[i][j] = matrix[i][j] || matrix[2 * position - i][j];
                matrix[2 * position - i][j] = false;
            }
        }
    }

    private static void foldY(boolean[][] matrix, int position) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < position; j++) {
                matrix[i][j] = matrix[i][j] || matrix[i][2 * position - j];
                matrix[i][2 * position - j] = false;
            }
        }
    }

    private static int countTrueValues(boolean[][] matrix) {
        int counter = 0;

        for (boolean[] booleans : matrix) {
            for (boolean aBoolean : booleans) {
                if (aBoolean) counter++;
            }
        }

        return counter;
    }
}
