import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day01 {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("data/day-01.txt");
        Scanner scanner = new Scanner(file);

//        int counter = partOne(scanner);
        int counter = partTwo(scanner);

        System.out.println("counter = " + counter);
    }

    private static int partOne(Scanner scanner) {
        int prev = scanner.nextInt();
        int counter = 0;

        while (scanner.hasNextInt()) {
            int value = scanner.nextInt();
            if (prev < value) {
                counter++;
            }
            prev = value;
        }
        return counter;
    }

    private static int partTwo(Scanner scanner) {
        int counter = 0;
        int[] sums = new int[3];
        int prev = Integer.MIN_VALUE;
        int j = 0;

        while (scanner.hasNextInt()) {
            int value = scanner.nextInt();
            for (int i = 0; i < sums.length; i++) {
                sums[i] += value;
            }

            if (j >= 3 && prev < sums[0]) {
                counter++;
            }

            prev = sums[0];

            sums[0] = sums[1];
            sums[1] = sums[2];
            sums[2] = 0;
            j++;
        }

        return counter;
    }
}
