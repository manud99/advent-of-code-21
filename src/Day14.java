import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Day14 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-14.txt"));

        String template = scanner.nextLine();
        Map<String, String> rules = new HashMap<>();

        scanner.nextLine();
        while (scanner.hasNextLine()) {
            String[] parts = scanner.nextLine().split(" -> ");
            rules.put(parts[0], parts[1]);
        }

        System.out.println("Part 1: " + partOne(template, rules));
        System.out.println("Part 2: " + partTwo(template, rules));
    }

    private static long partOne(String template, Map<String, String> rules) {
        Map<Character, Long> countMap = new HashMap<>();

        for (int step = 0; step < 10; step++) {
            for (int i = 0; i < template.length() - 1; i++) {
                String res = rules.get(template.substring(i, i + 2));
                if (res != null) {
                    template = template.substring(0, i + 1) + res + template.substring(i + 1);
                    i++;
                }
            }
        }

        for (char c : template.toCharArray()) {
            if (!countMap.containsKey(c)) {
                countMap.put(c, 0L);
            }

            countMap.put(c, countMap.get(c) + 1);
        }

        long min = countMap.values().stream().mapToLong(i -> i).min().orElse(0L);
        long max = countMap.values().stream().mapToLong(i -> i).max().orElse(0L);

        return max - min;
    }

    private static long partTwo(String template, Map<String, String> rules) {
        // count pairs
        Map<String, Long> pairs = new TreeMap<>();
        for (int i1 = 0; i1 < template.length() - 1; i1++) {
            String pair = template.substring(i1, i1 + 2);
            if (!pairs.containsKey(pair)) {
                pairs.put(pair, 0L);
            }
            pairs.put(pair, pairs.get(pair) + 1);
        }


        // for 40 steps ...
        for (int step = 0; step < 40; step++) {
            // for each pair
            Map<String, Long> anotherMap = new TreeMap<>(pairs);
            for (Map.Entry<String, Long> entry : pairs.entrySet()) {
                // if pair AB -> C exists in rules
                if (!rules.containsKey(entry.getKey())) continue;

                // increase amount AC and CB by amount of AB
                String res = rules.get(entry.getKey());
                increaseKey(anotherMap, entry.getKey().charAt(0) + res, entry.getValue());
                increaseKey(anotherMap, res + entry.getKey().charAt(1), entry.getValue());

                // AB does not exist anymore -> decrease key by its value ...
                increaseKey(anotherMap, entry.getKey(), -1 * entry.getValue());
            }
            pairs = anotherMap;
        }

        // count characters
        Map<Character, Long> countMap = new TreeMap<>();
        countMap.put(template.charAt(0), 1L);
        for (Map.Entry<String, Long> entry : pairs.entrySet()) {
            char c = entry.getKey().charAt(1);
            if (!countMap.containsKey(c)) {
                countMap.put(c, 0L);
            }

            countMap.put(c, countMap.get(c) + entry.getValue());
        }

        long min = countMap.values().stream().mapToLong(i -> i).min().orElse(0L);
        long max = countMap.values().stream().mapToLong(i -> i).max().orElse(0L);

        return max - min;
    }

    private static void increaseKey(Map<String, Long> pairs, String key, Long amount) {
        if (!pairs.containsKey(key)) {
            pairs.put(key, 0L);
        }

        pairs.put(key, pairs.get(key) + amount);
    }
}
