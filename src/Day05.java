import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day05 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-05.txt"));

        int result = partOne(scanner);
//        int result = partTwo(scanner);

        System.out.println("result = " + result);
    }

    private static int partOne(Scanner scanner) {
        int[][] matrix = new int[991][991];
        Pattern regex = Pattern.compile("(\\d+),(\\d+) -> (\\d+),(\\d+)");

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            Matcher matcher = regex.matcher(line);
            if (!matcher.matches()) break;

            int x1 = Integer.parseInt(matcher.group(1));
            int y1 = Integer.parseInt(matcher.group(2));
            int x2 = Integer.parseInt(matcher.group(3));
            int y2 = Integer.parseInt(matcher.group(4));

            if (x1 == x2) {
                for (int i = Math.min(y1, y2); i <= Math.max(y1, y2); i++) {
                    matrix[x1][i]++;
                }

            } else if (y1 == y2) {
                for (int i = Math.min(x1, x2); i <= Math.max(x1, x2); i++) {
                    matrix[i][y1]++;
                }

            } else {
                // part two
                if (x1 > x2) {
                    int temp = x1;
                    x1 = x2;
                    x2 = temp;
                    temp = y1;
                    y1 = y2;
                    y2 = temp;
                }

                int diff = x2 - x1;
                int slope = y1 > y2 ? -1 : 1;

                for (int i = 0; i <= diff; i++) {
                    matrix[x1 + i][y1 + i * slope]++;
                }
            }
        }

        int counter = 0;
        for (int[] rows : matrix) {
            for (int cell : rows) {
                if (cell >= 2) {
                    counter++;
                }
            }
        }

        return counter;
    }

    private static int partTwo(Scanner scanner) {
        return -1;
    }
}
