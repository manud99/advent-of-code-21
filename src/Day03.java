import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Day03 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-03.txt"));

//        int result = partOne(scanner);
        int result = partTwo(scanner);

        System.out.println("result = " + result);
    }

    private static int partOne(Scanner scanner) {
        int counter = 0;
        int[] occurrences = null;

        while (scanner.hasNextLine()) {
            // 010000010010
            String line = scanner.nextLine();
            char[] chars = line.toCharArray();
            counter++;

            if (occurrences == null) {
                occurrences = new int[chars.length];
            }

            for (int i = 0; i < chars.length; i++) {
                if (chars[i] == '1') {
                    occurrences[i]++;
                }
            }
        }

        StringBuilder gamma = new StringBuilder();
        StringBuilder epsilon = new StringBuilder();

        if (occurrences != null) {
            for (int occurrence : occurrences) {
                gamma.append(occurrence * 2 > counter ? '1' : '0');
                epsilon.append(occurrence * 2 <= counter ? '1' : '0');
            }
        }

        return Integer.parseInt(gamma.toString(), 2) * Integer.parseInt(epsilon.toString(), 2);
    }

    private static int partTwo(Scanner scanner) {
        int n = 12;
        ArrayList<char[]> list = new ArrayList<>(1000);

        while (scanner.hasNextLine()) {
            list.add(scanner.nextLine().toCharArray());
        }

        String oxygen = filterList(list, n, true);
        String co2 = filterList(list, n, false);

        return Integer.parseInt(oxygen, 2) * Integer.parseInt(co2, 2);
    }

    private static String filterList(ArrayList<char[]> list, int n, boolean filterOxygen) {
        list = new ArrayList<>(list);

        for (int i = 0; i < n; i++) {
            int occurrences = 0;

            for (char[] line : list) {
                if (line[i] == '1') {
                    occurrences++;
                } else {
                    occurrences--;
                }
            }

            int finalI = i;
            if ((filterOxygen && occurrences >= 0) || (!filterOxygen && occurrences < 0)) {
                list = list.stream().filter(lines -> lines[finalI] == '1').collect(Collectors.toCollection(ArrayList::new));
            } else {
                list = list.stream().filter(lines -> lines[finalI] == '0').collect(Collectors.toCollection(ArrayList::new));
            }

            if (list.size() == 1) {
                break;
            }
        }

        return new String(list.get(0));
    }
}
