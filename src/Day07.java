import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

interface Callable {
    int call(int distance);
}

public class Day07 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-07.txt"));
        int[] numbers = Arrays.stream(scanner.nextLine().split(",")).mapToInt(Integer::parseInt).toArray();

        System.out.println("Part one: " + partOne(numbers));
        System.out.println("Part two: " + partTwo(numbers));
    }

    private static int partOne(int[] numbers) {
        return algo(numbers, d -> d);
    }

    private static int partTwo(int[] numbers) {
        return algo(numbers, d -> d * (d + 1) / 2);
    }

    private static int algo(int[] numbers, Callable callable) {
        int min = Arrays.stream(numbers).min().orElse(0);
        int max = Arrays.stream(numbers).max().orElse(0);
        int bestFuel = Integer.MAX_VALUE;

        for (int i = min; i <= max; i++) {
            int fuel = 0;

            for (int number : numbers) {
                fuel += callable.call(Math.abs(i - number));
            }

            bestFuel = Math.min(bestFuel, fuel);
        }

        return bestFuel;
    }
}
