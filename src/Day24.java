import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Day24 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-24.txt"));
        ArrayList<Instruction>[] parts = new ArrayList[14];
        ArrayList<Instruction> part = null;
        int i = 0;

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            Scanner lineScanner = new Scanner(line);
            String fkt = lineScanner.next();
            String arg1 = lineScanner.next();
            String arg2 = null;
            if (lineScanner.hasNext()) arg2 = lineScanner.next();

            Instruction instr = new Instruction(fkt, arg1, arg2);

            if (instr.isInput() || part == null) {
                if (part != null) {
                    parts[i++] = part;
                }

                part = new ArrayList<>();
            }

            part.add(instr);
        }
        parts[i] = part;

        System.out.println("Part 1: " + partOne(parts));
        System.out.println("Part 2: " + partTwo(parts));
    }

    private static long partOne(ArrayList<Instruction>[] parts) {
        return predictLargestNumber(parts, 14, 0, 0);
    }

    private static long partTwo(ArrayList<Instruction>[] parts) {
        return predictSmallestNumber(parts, 14, 0, 0);
    }

    private static long predictLargestNumber(ArrayList<Instruction>[] parts, int step, int w, int z) {
        if (step != 14) {
            // 1 or 26
            int line4 = Integer.parseInt(parts[step].get(4).arg2);
            // -15...14 -> always > 10 when line4 == 1
            int line5 = Integer.parseInt(parts[step].get(5).arg2);
            // > 0
            int line15 = Integer.parseInt(parts[step].get(15).arg2);

            int res = z;
            if (line4 == 26) {
                z = res * 26 - line5 + w;
            } else {
                if ((res - line15 - w) % 26 != 0) return -1;

                z = (res - line15 - w) / 26;
            }
        }

        if (step == 0) {
            return w;
        }

        long max = -1;

        for (int i = 9; i >= 1; i--) {
            long res = predictLargestNumber(parts, step - 1, i, z);
            if (res != -1) max = Math.max(max, res);
        }

        if (step == 14) {
            return max;
        } else if (max == -1) {
            return -1;
        }

        return max * 10 + w;
    }

    private static long predictSmallestNumber(ArrayList<Instruction>[] parts, int step, int w, int z) {
        if (step != 14) {
            if (Integer.parseInt(parts[step].get(4).arg2) == 26) {
                z = z * 26 - Integer.parseInt(parts[step].get(5).arg2) + w;
            } else {
                int line15 = Integer.parseInt(parts[step].get(15).arg2);
                if ((z - line15 - w) % 26 != 0) return -1;

                z = (z - line15 - w) / 26;
            }
        }

        if (step == 0) {
            return w;
        }

        long min = Long.MAX_VALUE;

        for (int i = 1; i <= 9; i++) {
            long res = predictSmallestNumber(parts, step - 1, i, z);
            if (res != -1) min = Math.min(min, res);
        }

        if (step == 14) {
            return min;
        } else if (min == Long.MAX_VALUE) {
            return -1;
        }

        return min * 10 + w;
    }

    private static int predictZ(ArrayList<Instruction> program, int w, int z) {
        // in order to be 0 z need to be smaller than program.get(4).arg2 (= 26 or 1)
        int newZ = z / Integer.parseInt(program.get(4).arg2);

        // because of line5 >= 10 when line4 == 1 it isn't possible that z + line5 == w (w in {1..9})
        if ((z % 26) + Integer.parseInt(program.get(5).arg2) == w) {
            // only this line can return 0
            return newZ;
        }

        // res = 26 * (z / arg4) + w + arg15
        // z = (res - w - arg15) * arg4 / 26 (arg4 = 1 or 26)
        // implies when arg4 == 1 then (res - w - arg15) must be divisible by 26
        return 26 * newZ + w + Integer.parseInt(program.get(15).arg2);
    }

    private static class Instruction {
        private final String fkt;
        private final String arg1;
        private final String arg2;

        public Instruction(String fkt, String arg1, String arg2) {
            this.fkt = fkt;
            this.arg1 = arg1;
            this.arg2 = arg2;
        }

        public boolean isInput() {
            return this.fkt.equals("inp");
        }

        @Override
        public String toString() {
            return String.format("%s %s %s", fkt, arg1, arg2 == null ? "" : arg2);
        }
    }
}
