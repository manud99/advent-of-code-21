import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Day08 {

    private static final String[] SEGMENT_CODES = {"abcefg", "cf", "acdeg", "acdfg", "bcdf", "abdfg", "abdefg", "acf", "abcdefg", "abcdfg"};

    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-08.txt"));

        int resPartOne = 0;
        int resPartTwo = 0;

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] parts = line.split(" \\| ");
            String[] signalPatterns = parts[0].split(" ");
            String[] outputValues = parts[1].split(" ");

            resPartOne += partOne(outputValues);
            resPartTwo += partTwo(signalPatterns, outputValues);
        }

        System.out.println("Part 1: " + resPartOne);
        System.out.println("Part 2: " + resPartTwo);
    }

    private static int partOne(String[] outputValues) {
        int counter = 0;

        for (String value : outputValues) {
            if (value.length() == 2 || value.length() == 4 || value.length() == 3 || value.length() == 7) {
                counter++;
            }
        }

        return counter;
    }

    private static int partTwo(String[] signalPatterns, String[] outputValues) {
        char[] map = parseSignalPattern(signalPatterns);

        int sum = 0;
        for (int j = 0; j < outputValues.length; j++) {
            sum += parseValue(map, outputValues[j].toCharArray()) * Math.pow(10, outputValues.length - j - 1);
        }

        return sum;
    }

    private static char[] parseSignalPattern(String[] signalPatterns) {
        char[] map = new char[7];
        int[] occ = new int[7];
        String[] lengths = new String[8];
        char[][] lengthSix = new char[3][];
        int i = 0;

        for (String pattern : signalPatterns) {
            for (char c : pattern.toCharArray()) {
                occ[charToInt(c)]++;
            }
            lengths[pattern.length()] = pattern;
            if (pattern.length() == 6) {
                lengthSix[i++] = pattern.toCharArray();
            }
        }

        int b = findKey(occ, 6);
        int e = findKey(occ, 4);
        int d = findD(lengths[4].toCharArray(), lengths[2].toCharArray(), b);

        map[findKey(occ, 9)] = 'f';
        map[b] = 'b';
        map[e] = 'e';
        map[findDiff(lengths[3].toCharArray(), lengths[2].toCharArray())] = 'a';
        map[d] = 'd';
        map[findC(lengthSix, d, e)] = 'c';
        map[findG(map)] = 'g';

        return map;
    }

    private static int parseValue(char[] map, char[] value) {
        for (int i = 0; i < value.length; i++) {
            value[i] = map[charToInt(value[i])];
        }

        Arrays.sort(value);
        return findSegmentCode(new String(value));
    }

    private static int findD(char[] four, char[] one, int b) {
        for (char c : four) {
            if (notContains(one, c) && charToInt(c) != b) return charToInt(c);
        }

        return -1;
    }

    private static int findC(char[][] lengthSix, int d, int e) {
        int diff1 = findDiff(lengthSix[0], lengthSix[1]);
        int diff2 = findDiff(lengthSix[1], lengthSix[2]);
        int diff3 = findDiff(lengthSix[2], lengthSix[0]);

        if (diff1 != d && diff1 != e) {
            return diff1;
        } else if (diff2 != d && diff2 != e) {
            return diff2;
        }
        return diff3;
    }

    private static int findG(char[] map) {
        for (int i = 0; i < map.length; i++) {
            if (map[i] == Character.MIN_VALUE) return i;
        }
        return -1;
    }

    private static int findSegmentCode(String value) {
        for (int i = 0; i < SEGMENT_CODES.length; i++) {
            if (SEGMENT_CODES[i].equals(value)) return i;
        }

        return -1;
    }

    private static int findKey(int[] array, int value) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value) return i;
        }

        return -1;
    }

    private static int findDiff(char[] a, char[] b) {
        for (char c : a) {
            if (notContains(b, c)) return charToInt(c);
        }

        return -1;
    }

    private static boolean notContains(char[] arr, char b) {
        for (char c : arr) {
            if (c == b) return false;
        }
        return true;
    }

    private static int charToInt(char c) {
        return c - 97;
    }
}
