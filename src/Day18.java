import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Day18 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-18.txt"));
        ArrayList<String> list = new ArrayList<>();
        while (scanner.hasNextLine()) {
            list.add(scanner.nextLine());
        }
        String[] lines = list.toArray(String[]::new);

        System.out.println("Part 1: " + partOne(lines));
        System.out.println("Part 2: " + partTwo(lines));
    }

    private static int partOne(String[] lines) {
        // find final sum
        TreeNode result = null;

        for (String line : lines) {
            // make binary tree
            TreeNode currentTree = (TreeNode) buildTree(line, 0)[0];

            if (result == null) {
                result = currentTree;
                continue;
            }

            // add result + currentTree
            result = addTrees(result, currentTree);
        }

        return magnitude(result);
    }

    private static int partTwo(String[] lines) {
        int max = 0;

        for (String a : lines) {
            for (String b : lines) {
                if (a.equals(b)) continue;

                TreeNode rootA = (TreeNode) buildTree(a, 0)[0];
                TreeNode rootB = (TreeNode) buildTree(b, 0)[0];

                max = Math.max(max, magnitude(addTrees(rootA, rootB)));
            }
        }

        return max;
    }

    private static Object[] buildTree(String line, int pos) {
        char a = line.charAt(pos);
        int key = (int) a - '0';

        if (key >= 0 && key <= 9) {
            return new Object[]{new TreeNode(key), pos + 1};
        }

        Object[] objects = buildTree(line, pos + 1);
        TreeNode aa = (TreeNode) objects[0];
        pos = (int) objects[1] + 1; // inner position + 1 for the comma

        objects = buildTree(line, pos);
        TreeNode bb = (TreeNode) objects[0];
        pos = (int) objects[1] + 1; // inner position + 1 for "]"

        TreeNode treeNode = new TreeNode();
        treeNode.left = aa;
        aa.parent = treeNode;
        treeNode.right = bb;
        bb.parent = treeNode;

        return new Object[]{treeNode, pos};
    }

    private static TreeNode addTrees(TreeNode a, TreeNode b) {
        TreeNode sum = new TreeNode();
        sum.left = a;
        a.parent = sum;
        sum.right = b;
        b.parent = sum;

        relaxTree(sum);
        return sum;
    }

    private static void relaxTree(TreeNode root) {
        // find pair with depth > 4 then explode if not null
        TreeNode candidate = findExplodeCandidate(root, 1);
        if (candidate != null) {
            explode(candidate);
            relaxTree(root);
        }

        // has regular numbers >= 10 -> split -> restart
        candidate = findSplitCandidate(root);
        if (candidate != null) {
            split(candidate);
            relaxTree(root);
        }
    }

    private static TreeNode findExplodeCandidate(TreeNode node, int depth) {
        if (node == null || node.isRegularNumber()) {
            return null;
        }

        if (depth > 4) {
            return node;
        }

        TreeNode res = findExplodeCandidate(node.left, depth + 1);
        if (res != null) return res;

        return findExplodeCandidate(node.right, depth + 1);
    }

    private static void explode(TreeNode candidate) {
        int leftKey = candidate.left.key;
        int rightKey = candidate.right.key;

        if (leftKey < 0 || rightKey < 0)
            throw new RuntimeException("Candidate's child is not a number " + candidate.left + " OR " + candidate.right);

        // make candidate a regular number
        candidate.key = 0;
        candidate.right = null;
        candidate.left = null;

        // find next number to the left
        TreeNode leftNode = findNeighbour(candidate, true);
        if (leftNode != null) {
            leftNode.key += leftKey;
        }

        // find next number to the right
        TreeNode rightNode = findNeighbour(candidate, false);
        if (rightNode != null) {
            rightNode.key += rightKey;
        }
    }

    private static TreeNode findNeighbour(TreeNode node, boolean toTheLeft) {
        if (node.parent == null) {
            return null;
        }

        if (toTheLeft && node.parent.left != node) {
            node = node.parent.left;
            while (node.right != null) node = node.right;
            return node;

        } else if (!toTheLeft && node.parent.right != node) {
            node = node.parent.right;
            while (node.left != null) node = node.left;
            return node;
        }

        return findNeighbour(node.parent, toTheLeft);
    }

    private static TreeNode findSplitCandidate(TreeNode node) {
        if (node.isRegularNumber()) {
            if (node.key >= 10) {
                return node;
            }
            return null;
        }

        TreeNode res = findSplitCandidate(node.left);
        if (res != null) return res;

        return findSplitCandidate(node.right);
    }

    private static void split(TreeNode node) {
        node.left = new TreeNode((int) Math.floor(node.key / 2.0));
        node.left.parent = node;
        node.right = new TreeNode((int) Math.ceil(node.key / 2.0));
        node.right.parent = node;

        node.key = -1;
    }

    private static int magnitude(TreeNode node) {
        if (node == null) {
            return 0;
        }

        if (node.isRegularNumber()) {
            return node.key;
        }

        return magnitude(node.left) * 3 + magnitude(node.right) * 2;
    }
}

class TreeNode {
    int key;
    TreeNode parent;
    TreeNode left;
    TreeNode right;

    TreeNode() {
        // pair
        this.key = -1;
    }

    TreeNode(int key) {
        // regular number
        this.key = key;
    }

    public boolean isRegularNumber() {
        return this.key != -1;
    }

    @Override
    public String toString() {
        if (left != null && right != null) {
            return String.format("[%s,%s]", left, right);
        }

        return String.valueOf(key);
    }
}
