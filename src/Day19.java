import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Day19 {
    private final static int[][] permutations = new int[][]{
            {1, 0, 0, 0, 1, 0, 0, 0, 1},
            {1, 0, 0, 0, 1, 0, 0, 0, -1},
            {1, 0, 0, 0, -1, 0, 0, 0, 1},
            {1, 0, 0, 0, -1, 0, 0, 0, -1},
            {1, 0, 0, 0, 0, 1, 0, 1, 0},
            {1, 0, 0, 0, 0, 1, 0, -1, 0},
            {1, 0, 0, 0, 0, -1, 0, 1, 0},
            {1, 0, 0, 0, 0, -1, 0, -1, 0},
            {0, 1, 0, 1, 0, 0, 0, 0, 1},
            {0, 1, 0, 1, 0, 0, 0, 0, -1},
            {0, 1, 0, -1, 0, 0, 0, 0, 1},
            {0, 1, 0, -1, 0, 0, 0, 0, -1},
            {0, 1, 0, 0, 0, 1, 1, 0, 0},
            {0, 1, 0, 0, 0, 1, -1, 0, 0},
            {0, 1, 0, 0, 0, -1, 1, 0, 0},
            {0, 1, 0, 0, 0, -1, -1, 0, 0},
            {0, 0, 1, 1, 0, 0, 0, 1, 0},
            {0, 0, 1, 1, 0, 0, 0, -1, 0},
            {0, 0, 1, -1, 0, 0, 0, 1, 0},
            {0, 0, 1, -1, 0, 0, 0, -1, 0},
            {0, 0, 1, 0, 1, 0, 1, 0, 0},
            {0, 0, 1, 0, 1, 0, -1, 0, 0},
            {0, 0, 1, 0, -1, 0, 1, 0, 0},
            {0, 0, 1, 0, -1, 0, -1, 0, 0},
    };

    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-19.txt"));
        ArrayList<MyScanner> scanners = new ArrayList<>();
        int i = 0;

        while (scanner.hasNextLine()) {
            scanner.nextLine();
            ArrayList<Vector> points = new ArrayList<>();

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.isEmpty()) break;

                String[] parts = line.split(",");
                points.add(new Vector(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]), Integer.parseInt(parts[2])));
            }

            MyScanner myScanner = new MyScanner(i++, points);
            scanners.add(myScanner);
        }

        ArrayList<MyScanner> fixedScanners = new ArrayList<>();
        Set<Vector> absolutePoints = new HashSet<>(scanners.get(0).dataPoints);

        fixedScanners.add(scanners.get(0));
        scanners.remove(0);

        while (!scanners.isEmpty()) {
            for (ListIterator<MyScanner> itB = scanners.listIterator(); itB.hasNext(); ) {
                MyScanner scannerB = itB.next();

                for (ListIterator<MyScanner> itA = fixedScanners.listIterator(); itA.hasNext(); ) {
                    MyScanner scannerA = itA.next();

                    Set<Integer> matchingSquares = new TreeSet<>(scannerB.squares);
                    matchingSquares.retainAll(scannerA.squares);

                    if (matchingSquares.size() < 66) continue;

                    Vector[] points = getMatchingPoints(scannerA.vectors, scannerB.vectors);
                    int[] permutation = findPermutation(points);

                    Vector translation = new Vector(points[0], points[2].applyPermutation(permutation));

                    ArrayList<Vector> newPoints = scannerB.updatePoints(translation, permutation);
                    absolutePoints.addAll(newPoints);

                    itA.add(scannerB);
                    itB.remove();
                    break;
                }
            }
        }

        System.out.println("Part 1: " + absolutePoints.size());
        System.out.println("Part 2: " + maxDistance(fixedScanners));
    }

    private static Vector[] getMatchingPoints(ArrayList<ArrayList<Vector>> scannerA, ArrayList<ArrayList<Vector>> scannerB) {
        for (ArrayList<Vector> vectorsA : scannerA) {
            for (ArrayList<Vector> vectorsB : scannerB) {
                int counter = 0;
                Vector[] lastMatch = null;

                for (Vector vectorA : vectorsA) {
                    for (Vector vectorB : vectorsB) {
                        if (vectorA.length() == vectorB.length()) {
                            counter++;
                            lastMatch = new Vector[]{vectorA.from, vectorA.to, vectorB.from, vectorB.to};
                        }
                    }
                }

                if (counter == 11) {
                    return lastMatch;
                }
            }
        }

        throw new RuntimeException("No matching points found ...");
    }

    private static int[] findPermutation(Vector[] points) {
        Vector diffA = new Vector(points[0], points[1]);
        Vector diffB = new Vector(points[2], points[3]);

        for (int[] permutation : permutations) {
            if (diffA.equals(diffB.applyPermutation(permutation))) return permutation;
            if (diffA.equals(diffB.applyPermutation(permutation, -1))) {
                return Arrays.stream(permutation).map(i -> i * -1).toArray();
            }
        }

        return null;
    }

    private static int maxDistance(ArrayList<MyScanner> scanners) {
        int max = 0;
        for (int i = 0; i < scanners.size(); i++) {
            for (int j = i + 1; j < scanners.size(); j++) {
                max = Math.max(max, scanners.get(i).position.manhattanDistance(scanners.get(j).position));
            }
        }

        return max;
    }
}

class MyScanner {
    int key;
    ArrayList<Vector> dataPoints;
    Set<Integer> squares;
    ArrayList<ArrayList<Vector>> vectors;
    Vector position = new Vector(0, 0, 0);

    MyScanner(int key, ArrayList<Vector> dataPoints) {
        this.key = key;
        this.dataPoints = dataPoints;
        this.squares = calculateSquares(dataPoints);
        this.vectors = calculateVectors(dataPoints);
    }

    private Set<Integer> calculateSquares(ArrayList<Vector> dataPoints) {
        Set<Integer> list = new TreeSet<>();

        for (int i = 0; i < dataPoints.size(); i++) {
            for (int j = i + 1; j < dataPoints.size(); j++) {
                list.add(dataPoints.get(i).distance(dataPoints.get(j)));
            }
        }

        return list;
    }

    private ArrayList<ArrayList<Vector>> calculateVectors(ArrayList<Vector> dataPoints) {
        ArrayList<ArrayList<Vector>> result = new ArrayList<>();
        for (Vector from : dataPoints) {
            ArrayList<Vector> list = new ArrayList<>(25);

            for (Vector to : dataPoints) {
                if (from.equals(to)) continue;

                list.add(new Vector(from, to));
            }

            result.add(list);
        }

        return result;
    }

    public ArrayList<Vector> updatePoints(Vector translation, int[] permutation) {
        this.position = translation.negate();

        ArrayList<Vector> newPoints = new ArrayList<>();
        for (Vector dataPoint : this.dataPoints) {
            newPoints.add(dataPoint.applyPermutation(permutation).sub(translation));
        }

        dataPoints = newPoints;
        vectors = calculateVectors(newPoints);

        return newPoints;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("Scanner " + key + ": ");

        if (position != null) {
            builder.append(" ").append(position);
        }

        return builder.toString();
    }
}

class Vector {
    final int x;
    final int y;
    final int z;
    final Vector from;
    final Vector to;

    public Vector(int x, int y, int z, Vector from, Vector to) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.from = from;
        this.to = to;
    }

    public Vector(int x, int y, int z) {
        this(x, y, z, null, null);
    }

    Vector(Vector from, Vector to) {
        this(to.x - from.x, to.y - from.y, to.z - from.z, from, to);
    }

    public int length() {
        return (int) (Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    }

    public int distance(Vector p) {
        return p.sub(this).length();
    }

    public int manhattanDistance(Vector other) {
        return Math.abs(this.x - other.x) + Math.abs(this.y - other.y) + Math.abs(this.z - other.z);
    }

    public Vector applyPermutation(int[] permutation) {
        return applyPermutation(permutation, 1);
    }

    public Vector applyPermutation(int[] permutation, int factor) {
        int x = (permutation[0] * this.x + permutation[1] * this.y + permutation[2] * this.z) * factor;
        int y = (permutation[3] * this.x + permutation[4] * this.y + permutation[5] * this.z) * factor;
        int z = (permutation[6] * this.x + permutation[7] * this.y + permutation[8] * this.z) * factor;
        return new Vector(x, y, z, from, to);
    }

    public Vector add(Vector v) {
        return new Vector(x + v.x, y + v.y, z + v.z);
    }

    public Vector sub(Vector v) {
        return new Vector(x - v.x, y - v.y, z - v.z);
    }

    public Vector negate() {
        return new Vector(x * -1, y * -1, z * -1, from, to);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector vector = (Vector) o;
        return (x == vector.x && y == vector.y && z == vector.z);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }

    @Override
    public String toString() {
        return String.format("%d/%d/%d -> %d", x, y, z, length());
    }
}
