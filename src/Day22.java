import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day22 {
    static class RebootStep {
        int x1, x2, y1, y2, z1, z2;
        boolean turnsOn;

        public RebootStep(int x1, int x2, int y1, int y2, int z1, int z2, boolean turnsOn) {
            this.x1 = x1;
            this.x2 = x2;
            this.y1 = y1;
            this.y2 = y2;
            this.z1 = z1;
            this.z2 = z2;
            this.turnsOn = turnsOn;
        }

        public boolean outside50() {
            return x2 < -50 || x1 > 50 || y2 < -50 || y1 > 50 || z2 < -50 || z1 > 50;
        }

        public boolean intersects(Cube cube) {
            return ((x1 <= cube.x1 && cube.x1 <= x2) || (x1 <= cube.x2 && cube.x2 <= x2) || (x1 >= cube.x1 && x2 <= cube.x2))
                    && ((y1 <= cube.y1 && cube.y1 <= y2) || (y1 <= cube.y2 && cube.y2 <= y2) || (y1 >= cube.y1 && y2 <= cube.y2))
                    && ((z1 <= cube.z1 && cube.z1 <= z2) || (z1 <= cube.z2 && cube.z2 <= z2) || (z1 >= cube.z1 && z2 <= cube.z2));
        }

        public List<Cube> cutOffCubes(Cube cube) {
            List<Cube> cubes = new ArrayList<>();

            // cut off left x
            if (cube.x1 < this.x1) {
                cubes.add(new Cube(cube.x1, this.x1 - 1, cube.y1, cube.y2, cube.z1, cube.z2));
            }
            // cut off right x
            if (cube.x2 > this.x2) {
                cubes.add(new Cube(this.x2 + 1, cube.x2, cube.y1, cube.y2, cube.z1, cube.z2));
            }

            // inside x
            int x1 = Math.max(this.x1, cube.x1);
            int x2 = Math.min(this.x2, cube.x2);

            // cut off left y
            if (cube.y1 < this.y1) {
                cubes.add(new Cube(x1, x2, cube.y1, this.y1 - 1, cube.z1, cube.z2));
            }
            // cut off right y
            if (cube.y2 > this.y2) {
                cubes.add(new Cube(x1, x2, this.y2 + 1, cube.y2, cube.z1, cube.z2));
            }

            // inside x and y
            int y1 = Math.max(this.y1, cube.y1);
            int y2 = Math.min(this.y2, cube.y2);

            // cut off left z
            if (cube.z1 < this.z1) {
                cubes.add(new Cube(x1, x2, y1, y2, cube.z1, this.z1 - 1));
            }
            // cut off right z
            if (cube.z2 > this.z2) {
                cubes.add(new Cube(x1, x2, y1, y2, this.z2 + 1, cube.z2));
            }

            return cubes;
        }

        public Cube toCube() {
            return new Cube(x1, x2, y1, y2, z1, z2);
        }

        @Override
        public String toString() {
            return String.format("Step %d %d %d %d %d %d turnsOn: %s}", x1, x2, y1, y2, z1, z2, turnsOn);
        }
    }

    static class Cube {
        int x1, x2, y1, y2, z1, z2;

        public Cube(int x1, int x2, int y1, int y2, int z1, int z2) {
            this.x1 = x1;
            this.x2 = x2;
            this.y1 = y1;
            this.y2 = y2;
            this.z1 = z1;
            this.z2 = z2;
        }

        public long size() {
            return (long) (x2 - x1 + 1) * (y2 - y1 + 1) * (z2 - z1 + 1);
        }

        @Override
        public String toString() {
            return String.format("Cube %d %d %d %d %d %d}", x1, x2, y1, y2, z1, z2);
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-22.txt"));
        Pattern pattern = Pattern.compile("(on|off) x=(-?\\d+)..(-?\\d+),y=(-?\\d+)..(-?\\d+),z=(-?\\d+)..(-?\\d+)");
        ArrayList<RebootStep> rebootSteps = new ArrayList<>();

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            Matcher matcher = pattern.matcher(line);
            matcher.matches();

            RebootStep rebootStep = new RebootStep(
                    Integer.parseInt(matcher.group(2)),
                    Integer.parseInt(matcher.group(3)),
                    Integer.parseInt(matcher.group(4)),
                    Integer.parseInt(matcher.group(5)),
                    Integer.parseInt(matcher.group(6)),
                    Integer.parseInt(matcher.group(7)),
                    matcher.group(1).equals("on")
            );
            rebootSteps.add(rebootStep);
        }

        System.out.println("Part 1: " + partOne(rebootSteps));
        System.out.println("Part 2: " + partTwo(rebootSteps));
    }

    private static int partOne(ArrayList<RebootStep> steps) {
        boolean[][][] cubes = new boolean[101][101][101];

        for (RebootStep step : steps) {
            if (step.outside50()) continue;

            for (int x = Math.max(-50, step.x1); x <= Math.min(50, step.x2); x++) {
                for (int y = Math.max(-50, step.y1); y <= Math.min(50, step.y2); y++) {
                    for (int z = Math.max(-50, step.z1); z <= Math.min(50, step.z2); z++) {
                        cubes[x + 50][y + 50][z + 50] = step.turnsOn;
                    }
                }
            }
        }

        int counter = 0;
        for (boolean[][] cubeX : cubes) {
            for (boolean[] cubeY : cubeX) {
                for (boolean cubeZ : cubeY) {
                    if (cubeZ) counter++;
                }
            }
        }

        return counter;
    }

    private static long partTwo(ArrayList<RebootStep> steps) {
        ArrayList<Cube> onCubes = new ArrayList<>();

        for (RebootStep step : steps) {
            ArrayList<Cube> newOnCubes = new ArrayList<>();
            if (step.turnsOn) {
                newOnCubes.add(step.toCube());
            }

            for (Cube cube : onCubes) {
                if (step.intersects(cube)) {
                    newOnCubes.addAll(step.cutOffCubes(cube));
                } else {
                    newOnCubes.add(cube);
                }
            }
            onCubes = newOnCubes;
        }

        long counter = 0;
        for (Cube cube : onCubes) {
            counter += cube.size();
        }

        return counter;
    }
}
