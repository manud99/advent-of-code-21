import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Day20 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-20.txt"));

        String algorithm = scanner.nextLine();
        scanner.nextLine();
        ArrayList<String> lines = new ArrayList<>();
        while (scanner.hasNextLine()) {
            lines.add(scanner.nextLine());
        }
        boolean[][] input = new boolean[lines.size()][lines.get(0).length()];
        for (int i = 0; i < lines.size(); i++) {
            char[] line = lines.get(i).toCharArray();
            for (int j = 0; j < line.length; j++) {
                input[i][j] = line[j] == '#';
            }
        }

        System.out.println("Part 1: " + partOne(algorithm, input));
        System.out.println("Part 2: " + partTwo(algorithm, input));
    }

    private static int partOne(String algorithm, boolean[][] input) {
        boolean[][] output = enhance(algorithm, input, 2);

        return countTrueValues(output);
    }

    private static int partTwo(String algorithm, boolean[][] input) {
        boolean[][] output = enhance(algorithm, input, 50);

        return countTrueValues(output);
    }

    private static boolean[][] enhance(String algorithm, boolean[][] input, int steps) {
        boolean[][] output = input;

        for (int step = 0; step < steps; step++) {
            boolean surrounding = algorithm.charAt(0) == '#' && step % 2 == 1;
            input = extendMatrix(input, surrounding);
            output = deepCopy(input);

            for (int i = 0; i < input.length; i++) {
                for (int j = 0; j < input[i].length; j++) {
                    String binary = findBinary(input, i, j, surrounding);
                    int decimal = Integer.parseInt(binary, 2);
                    output[i][j] = algorithm.charAt(decimal) == '#';
                }
            }

            input = output;
        }
        return output;
    }

    private static int countTrueValues(boolean[][] output) {
        int counter = 0;
        for (boolean[] booleans : output) {
            for (boolean aBoolean : booleans) {
                if (aBoolean) counter++;
            }
        }
        return counter;
    }

    private static boolean[][] extendMatrix(boolean[][] input, boolean surrounding) {
        boolean[][] result = new boolean[input.length + 2][input[0].length + 2];
        Arrays.fill(result[0], surrounding);
        Arrays.fill(result[input.length + 1], surrounding);

        for (int i = 0; i < input.length; i++) {
            result[i + 1][0] = surrounding;
            System.arraycopy(input[i], 0, result[i + 1], 1, input[i].length);
            result[i + 1][input[0].length + 1] = surrounding;
        }

        return result;
    }

    private static boolean[][] deepCopy(boolean[][] original) {
        final boolean[][] result = new boolean[original.length][];

        for (int i = 0; i < original.length; i++) {
            result[i] = Arrays.copyOf(original[i], original[i].length);
        }

        return result;
    }

    private static String findBinary(boolean[][] matrix, int x, int y, boolean surrounding) {
        StringBuilder builder = new StringBuilder();

        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                if (i < 0 || j < 0 || i >= matrix.length || j >= matrix[i].length) {
                    builder.append(surrounding ? "1" : "0");
                    continue;
                }

                builder.append(matrix[i][j] ? "1" : "0");
            }
        }

        return builder.toString();
    }

    private static String printArray(boolean[][] matrix) {
        StringBuilder builder = new StringBuilder();

        for (boolean[] booleans : matrix) {
            for (boolean aBoolean : booleans) {
                builder.append(aBoolean ? "#" : ".");
            }
            builder.append("\n");
        }

        return builder.toString();
    }
}
