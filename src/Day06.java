import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Day06 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-06.txt"));

        // int result = partOne(scanner);
        long result = partTwo(scanner);

        System.out.println("result = " + result);
    }

    private static int partOne(Scanner scanner) {
        String[] numbers = scanner.nextLine().split(",");
        ArrayList<Fish> fishes = new ArrayList<>(numbers.length);

        for (String number : numbers) {
            fishes.add(new Fish(Integer.parseInt(number)));
        }

        for (int i = 0; i < 80; i++) {
            int size = fishes.size();
            for (int j = 0; j < size; j++) {
                boolean newFish = fishes.get(j).nextRound();
                if (newFish) {
                    fishes.add(new Fish(8));
                }
            }
        }

        return fishes.size();
    }

    private static long partTwo(Scanner scanner) {
        String[] numbers = scanner.nextLine().split(",");
        long[] days = new long[9];

        for (String number : numbers) {
            days[Integer.parseInt(number)]++;
        }

        for (int i = 0; i < 256; i++) {
            long day0 = days[0];
            for (int j = 0; j < 8; j++) {
                days[j] = days[j + 1];
            }
            days[6] += day0;
            days[8] = day0;
        }

        return Arrays.stream(days).sum();
    }
}

class Fish {
    private int n;

    Fish(int n) {
        this.n = n;
    }

    public boolean nextRound() {
        this.n--;

        if (this.n == -1) {
            this.n = 6;
            return true;
        }

        return false;
    }
}
