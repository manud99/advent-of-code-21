import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Day11 {
    private static final int n = 10;

    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-11.txt"));

        int[][] matrix = new int[n][n];
        for (int i = 0; scanner.hasNextLine(); i++) {
            char[] chars = scanner.nextLine().toCharArray();
            for (int j = 0; j < chars.length; j++) {
                matrix[i][j] = chars[j] - 48;
            }
        }

        System.out.println("Part 1: " + partOne(deepCopy(matrix)));
        System.out.println("Part 2: " + partTwo(deepCopy(matrix)));
    }

    private static int partOne(int[][] matrix) {
        int flashes = 0;

        for (int step = 1; step <= 100; step++) {
            // increase all numbers by one
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    matrix[i][j]++;
                }
            }

            // while there occur flashes ...
            boolean flashOccurred;
            do {
                flashOccurred = flash(matrix);
                if (flashOccurred) {
                    flashes++;
                }
            } while (flashOccurred);
        }

        return flashes;
    }

    private static int partTwo(int[][] matrix) {
        for (int step = 1; true; step++) {
            // increase all numbers by one
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    matrix[i][j]++;
                }
            }

            // while there occur flashes ...
            boolean flashOccurred;
            int flashes = 0;
            do {
                flashOccurred = flash(matrix);
                if (flashOccurred) {
                    flashes++;
                }
            } while (flashOccurred);

            if (flashes == n * n) {
                return step;
            }
        }
    }

    private static boolean flash(int[][] matrix) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] > 9) {
                    matrix[i][j] = 0;

                    propagateFlash(matrix, i - 1, j - 1);
                    propagateFlash(matrix, i - 1, j);
                    propagateFlash(matrix, i - 1, j + 1);
                    propagateFlash(matrix, i, j - 1);
                    propagateFlash(matrix, i, j + 1);
                    propagateFlash(matrix, i + 1, j - 1);
                    propagateFlash(matrix, i + 1, j);
                    propagateFlash(matrix, i + 1, j + 1);

                    return true;
                }
            }
        }

        return false;
    }

    private static void propagateFlash(int[][] matrix, int x, int y) {
        if (x < 0 || y < 0 || x >= n || y >= n) {
            return;
        }

        if (matrix[x][y] > 0) {
            matrix[x][y]++;
        }
    }

    private static void printMatrix(int[][] matrix, int step) {
        for (int i = 0; i < n; i++) {
            if (i == 0) {
                System.out.printf("%03d = ", step);
            } else {
                System.out.print("      ");
            }
            for (int j = 0; j < n; j++) {
                System.out.print(matrix[i][j]);
            }
            System.out.println();
        }
    }

    public static int[][] deepCopy(int[][] original) {
        if (original == null) {
            return null;
        }

        final int[][] result = new int[original.length][];
        for (int i = 0; i < original.length; i++) {
            result[i] = Arrays.copyOf(original[i], original[i].length);
        }
        return result;
    }
}
