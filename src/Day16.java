import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.stream.LongStream;

public class Day16 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-16.txt"));

        while (scanner.hasNextLine()) {
            String code = scanner.nextLine();

            Day16 d16 = new Day16(code);
            System.out.println("Part 1: " + d16.partOne());

            d16 = new Day16(code);
            System.out.println("Part 2: " + d16.partTwo());
        }
    }

    int pointer;
    String bin;

    Day16(String code) {
        this.bin = parseCode(code);
    }

    private int partOne() {
        return parsePackage();
    }

    private long partTwo() {
        return evalPackage();
    }

    private String parseCode(String code) {
        StringBuilder builder = new StringBuilder();

        for (char c : code.toCharArray()) {
            int i = Integer.parseInt(String.valueOf(c), 16);
            String str = String.format("%4s", Integer.toBinaryString(i)).replace(' ', '0');
            builder.append(str);
        }

        return builder.toString();
    }

    private int parsePackage() {
        int version = Integer.parseInt(next(3), 2);
        int typeId = Integer.parseInt(next(3), 2);

        if (typeId == 4) {
            parseLiteral();
        } else {
            if (next(1).equals("0")) {
                int length = Integer.parseInt(next(15), 2);
                int end = this.pointer + length;
                while (this.pointer < end) {
                    version += parsePackage();
                }
            } else {
                int numberSubpackages = Integer.parseInt(next(11), 2);

                for (int i = 0; i < numberSubpackages; i++) {
                    version += parsePackage();
                }
            }
        }

        return version;
    }

    private long evalPackage() {
        next(3); // version
        int typeId = Integer.parseInt(next(3), 2);

        if (typeId == 4) {
            return evalLiteral();
        }

        ArrayList<Long> list = evalSubPackages();
        LongStream stream = list.stream().mapToLong(i -> i);

        switch (typeId) {
            case 0:
                return stream.sum();

            case 1:
                return stream.reduce(1, (a, b) -> a * b);

            case 2:
                return stream.min().getAsLong();

            case 3:
                return stream.max().getAsLong();

            case 5:
                return list.get(0) > list.get(1) ? 1 : 0;

            case 6:
                return list.get(0) < list.get(1) ? 1 : 0;

            case 7:
                return list.get(0).equals(list.get(1)) ? 1 : 0;
        }

        throw new RuntimeException("Unexpected typeId " + typeId);
    }

    private ArrayList<Long> evalSubPackages() {
        ArrayList<Long> list = new ArrayList<>();

        if (next(1).equals("0")) {
            int length = Integer.parseInt(next(15), 2);
            int end = this.pointer + length;

            while (this.pointer < end) {
                list.add(evalPackage());
            }

        } else {
            int numberSubpackages = Integer.parseInt(next(11), 2);

            for (int i = 0; i < numberSubpackages; i++) {
                list.add(evalPackage());
            }
        }

        return list;
    }

    private String parseLiteral() {
        String next;
        StringBuilder builder = new StringBuilder();

        do {
            next = next(5);
            builder.append(next.substring(1));
        } while (next.startsWith("1"));

        return builder.toString();
    }

    private long evalLiteral() {
        return Long.parseLong(parseLiteral(), 2);
    }

    private String next(int length) {
        String res = bin.substring(pointer, pointer + length);
        pointer += length;
        return res;
    }
}
