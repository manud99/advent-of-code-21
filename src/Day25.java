import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Day25 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-25.txt"));
        int[][] matrix = new int[137][139];

        for (int i = 0; scanner.hasNextLine(); i++) {
            char[] line = scanner.nextLine().toCharArray();
            for (int j = 0; j < line.length; j++) {
                if (line[j] == 'v') {
                    matrix[i][j] = 1;
                } else if (line[j] == '>') {
                    matrix[i][j] = 2;
                }
            }
        }

        System.out.println("Part 1: " + partOne(deepCopy(matrix)));
    }

    private static int partOne(int[][] matrix) {
        boolean nobodyMoved;
        int step = 0;

        do {
            nobodyMoved = true;
            step++;

            // first look row-wise from left to right
            for (int i = 0; i < matrix.length; i++) {
                boolean firstWasEmpty = matrix[i][0] == 0;

                for (int j = 0; j < matrix[i].length; j++) {
                    if (matrix[i][j] != 2) continue;

                    if (j + 1 == matrix[i].length) {
                        if (!firstWasEmpty) continue;

                        matrix[i][j] = 0;
                        matrix[i][0] = 2;
                        j++;
                        nobodyMoved = false;

                    } else if (matrix[i][j + 1] == 0) {
                        matrix[i][j] = 0;
                        matrix[i][j + 1] = 2;
                        j++;
                        nobodyMoved = false;
                    }
                }
            }

            // then look column-wise top down
            for (int j = 0; j < matrix[0].length; j++) {
                boolean firstWasEmpty = matrix[0][j] == 0;

                for (int i = 0; i < matrix.length; i++) {
                    if (matrix[i][j] != 1) continue;

                    if (i + 1 == matrix.length) {
                        if (!firstWasEmpty) continue;

                        matrix[i][j] = 0;
                        matrix[0][j] = 1;
                        i++;
                        nobodyMoved = false;

                    } else if (matrix[i + 1][j] == 0) {
                        matrix[i][j] = 0;
                        matrix[i + 1][j] = 1;
                        i++;
                        nobodyMoved = false;
                    }
                }
            }
        } while (!nobodyMoved);

        return step;
    }

    private static void printMatrix(int[][] matrix) {
        for (int[] ints : matrix) {
            for (int anInt : ints) {
                if (anInt == 0) System.out.print('.');
                else if (anInt == 1) System.out.print('v');
                else if (anInt == 2) System.out.print('>');
                else System.out.print('?');
            }
            System.out.println();
        }
        System.out.println();
    }

    private static int[][] deepCopy(int[][] original) {
        int[][] res = new int[original.length][];
        for (int i = 0; i < original.length; i++) {
            res[i] = Arrays.copyOf(original[i], original[i].length);
        }
        return res;
    }
}
