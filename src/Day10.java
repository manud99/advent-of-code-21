import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Day10 {
    public static void main(String[] args) throws FileNotFoundException {
        String file = "data/day-10.txt";
        Scanner scanner = new Scanner(new File(file));
        System.out.println("Part 1: " + partOne(scanner));
        scanner.close();

        scanner = new Scanner(new File(file));
        System.out.println("Part 2: " + partTwo(scanner));
        scanner.close();
    }

    private static int partOne(Scanner scanner) {
        int score = 0;

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();

            // check if corrupt and find corrupt symbol
            int symbol = findCorruptSymbol(line.toCharArray(), 0);

            if (symbol <= 0) {
                continue;
            }

            // calculate score
            switch ((char) symbol) {
                case ')':
                    score += 3;
                    break;
                case ']':
                    score += 57;
                    break;
                case '}':
                    score += 1197;
                    break;
                case '>':
                    score += 25137;
                    break;
            }
        }

        return score;
    }

    private static int findCorruptSymbol(char[] line, int start) {
        char openingChar = line[start];
        for (int i = start + 1; i < line.length; i++) {
            char c = line[i];
            if (c == '(' || c == '[' || c == '{' || c == '<') {
                int error = findCorruptSymbol(line, i);

                if (error >= 0) {
                    return error;
                } else {
                    i = error * -1;
                }
            } else if (isClosingChar(openingChar, c)) {
                return i * -1;
            } else {
                return c;
            }
        }

        return 0;
    }

    private static boolean isClosingChar(char open, char close) {
        switch (open) {
            case '(':
                return close == ')';
            case '[':
                return close == ']';
            case '{':
                return close == '}';
            case '<':
                return close == '>';
        }
        return false;
    }

    private static long partTwo(Scanner scanner) {
        ArrayList<Long> scores = new ArrayList<>();

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();

            try {
                int i = 0;
                long res = 0;

                while (i < line.length() - 1) {
                    res = findIncompleteLines(line.toCharArray(), i);
                    if (res < 0) {
                        i = (int) res * -1 + 1;
                    } else {
                        i = line.length();
                    }
                }

                if (res <= 0) {
                    continue;
                }

                scores.add(res);
            } catch (Exception e) {
                // go to next entry ...
            }
        }

        Collections.sort(scores);

        if (scores.size() == 0) {
            return 0;
        }

        return scores.get(scores.size() / 2);
    }

    private static long findIncompleteLines(char[] line, int start) throws Exception {
        char openingChar = line[start];
        long score = 0;

        for (int i = start + 1; i < line.length; i++) {
            char c = line[i];
            if (c == '(' || c == '[' || c == '{' || c == '<') {
                long res = findIncompleteLines(line, i);

                if (res > 0) {
                    score = res;
                    break;

                } else if (res < 0) {
                    i = (int) res * -1;
                }
            } else if (isClosingChar(openingChar, c)) {
                return i * -1;
            } else {
                throw new Exception("Corrupt exception.");
            }
        }

        score *= 5;
        switch (openingChar) {
            case '(':
                score += 1;
                break;
            case '[':
                score += 2;
                break;
            case '{':
                score += 3;
                break;
            case '<':
                score += 4;
                break;
        }

        return score;
    }
}
