import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day21 {
    private int round = 0;
    private int dice = 1;
    private final int[] probabilities = new int[]{1, 3, 6, 7, 6, 3, 1};
    private final long[][][][][][] memo = new long[11][11][32][32][2][];

    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-21.txt"));

        int position1 = getStartingPosition(scanner.nextLine());
        int position2 = getStartingPosition(scanner.nextLine());

        Day21 game = new Day21();
        System.out.println("Part 1: " + game.partOne(position1, position2));
        game.reset();

        long[] universes = game.partTwo(new int[]{position1, position2}, new int[]{0, 0}, true);
        System.out.println("universes = " + Arrays.toString(universes));
        System.out.println("Part 2: " + Arrays.stream(universes).max().orElse(0));
    }

    private int partOne(int position1, int position2) {
        int score1 = 0;
        int score2 = 0;

        while (score2 < 1000) {
            position1 = nextRound(position1, playDice());
            score1 += position1;

            if (score1 >= 1000) break;

            position2 = nextRound(position2, playDice());
            score2 += position2;
        }

        return this.round * Math.min(score1, score2);
    }

    private long[] partTwo(int[] positions, int[] scores, boolean playerOnesTurn) {
        if (memo[positions[0]][positions[1]][scores[0]][scores[1]][playerOnesTurn ? 1 : 0] != null) {
            return memo[positions[0]][positions[1]][scores[0]][scores[1]][playerOnesTurn ? 1 : 0];
        }

        if (scores[0] >= 21) {
            return new long[]{1, 0};
        } else if (scores[1] >= 21) {
            return new long[]{0, 1};
        }

        long[] universes = new long[]{0, 0};

        for (int i = 0; i < probabilities.length; i++) {
            int[] pos = Arrays.copyOf(positions, positions.length);
            int[] sco = Arrays.copyOf(scores, scores.length);
            int cur = playerOnesTurn ? 0 : 1;

            pos[cur] = nextRound(pos[cur], i + 3);
            sco[cur] += pos[cur];
            int probability = probabilities[i];

            long[] result = partTwo(pos, sco, !playerOnesTurn);
            universes[0] += result[0] * probability;
            universes[1] += result[1] * probability;
        }

        memo[positions[0]][positions[1]][scores[0]][scores[1]][playerOnesTurn ? 1 : 0] = universes;

        return universes;
    }

    public void reset() {
        this.round = 0;
        this.dice = 1;
    }

    private int nextRound(int position, int diceSum) {
        this.round += 3;

        position += diceSum;
        position %= 10;
        if (position == 0) position = 10;

        return position;
    }

    private int playDice() {
        int res = 0;

        for (int i = 0; i < 3; i++) {
            res += this.dice++;
            if (this.dice > 100) this.dice = 1;
        }

        return res;
    }

    private static int getStartingPosition(String line) {
        Pattern pattern = Pattern.compile("Player \\d starting position: (\\d+)");
        Matcher matcher = pattern.matcher(line);

        if (matcher.matches()) return Integer.parseInt(matcher.group(1));
        return -1;
    }
}
