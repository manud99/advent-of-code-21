import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Day12 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("data/day-12.txt"));

        Map<String, Cave> caves = parseCaves(scanner);

        System.out.println("Part 1: " + partOne(caves));
        System.out.println("Part 2: " + partTwo(caves));
    }

    private static Map<String, Cave> parseCaves(Scanner scanner) {
        Map<String, Cave> caves = new HashMap<>();

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] parts = line.split("-");

            Cave u = getCave(caves, parts[0]);
            Cave v = getCave(caves, parts[1]);

            u.adjList.add(v);
            v.adjList.add(u);
        }

        return caves;
    }

    private static Cave getCave(Map<String, Cave> caves, String name) {
        Cave u = caves.get(name);

        if (u == null) {
            u = new Cave(name);
            caves.put(name, u);
        }

        return u;
    }

    private static int partOne(Map<String, Cave> caves) {
        return findNumberOfPaths(caves.get("start"), new ArrayList<>());
    }

    private static int findNumberOfPaths(Cave cave, ArrayList<Cave> path) {
        if (cave.name.equals("end")) {
            return 1;
        }

        int res = 0;
        path.add(cave);

        for (Cave c : cave.adjList) {
            if (!c.big && path.contains(c)) {
                continue;
            }

            res += findNumberOfPaths(c, new ArrayList<>(path));
        }

        return res;
    }

    private static int partTwo(Map<String, Cave> caves) {
        return findNumberOfPathsPartTwo(caves.get("start"), new ArrayList<>(), false);
    }

    private static int findNumberOfPathsPartTwo(Cave cave, ArrayList<Cave> path, boolean hasSecondVisit) {
        if (cave.name.equals("end")) {
            return 1;
        }

        int res = 0;
        path.add(cave);

        for (Cave c : cave.adjList) {
            if (!canBeVisited(c, path, hasSecondVisit)) {
                continue;
            }

            res += findNumberOfPathsPartTwo(c, new ArrayList<>(path), hasSecondVisit || (!c.big && path.contains(c)));
        }

        return res;
    }

    private static boolean canBeVisited(Cave cave, ArrayList<Cave> path, boolean hasSecondVisit) {
        if (cave.big || cave.name.equals("end")) {
            return true;
        }

        if (cave.name.equals("start")) {
            return false;
        }

        if (hasSecondVisit) {
            return !path.contains(cave);
        }

        return true;
    }

    private static void printPath(ArrayList<Cave> path) {
        for (Cave cave : path) {
            System.out.print(cave.name + ",");
        }
        System.out.println("end");
    }
}

class Cave {
    String name;
    boolean big;
    ArrayList<Cave> adjList = new ArrayList<>();

    Cave(String name) {
        this.name = name;
        this.big = (int) name.charAt(0) < 91;
    }
}
